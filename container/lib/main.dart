import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatefulWidget {
  const MainApp({super.key});

  @override
  State createState() => _MainApp();
}

class _MainApp extends State {
  int count = 1;
  bool flag = true;

  void nextQuestion() {
    setState(() {
      count++;
    });
  }

  Scaffold question() {
    if (count == 1) {
      return Scaffold(
        body: Center(
          child: Container(
            height: 300,
            width: 300,
            color: Colors.amber,
            padding: EdgeInsets.all(25),
            child: Image.network(
                "https://www.freecodecamp.org/news/content/images/2021/08/imgTag.png"),
          ),
        ),
      );
    } else if (count == 2) {
      return Scaffold(
        body: Center(
          child: Container(
              width: 300,
              height: 300,
              decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: NetworkImage(
                          "https://i.pinimg.com/474x/42/2b/b2/422bb20ca8140882840656315ad2e1de.jpg"))),
              child: const Center(
                child: Text(
                  "Core2web",
                  style: TextStyle(color: Colors.amber),
                ),
              )),
        ),
      );
    } else if (count == 3) {
      return Scaffold(
        body: Center(
            child: GestureDetector(
          onTap: () {
            setState(() {
              flag = false;
            });
          },
          child: Container(
            height: 200,
            width: 300,
            decoration: BoxDecoration(
                color: Colors.amber,
                border: Border.all(color: (flag) ? Colors.black : Colors.red)),
          ),
        )),
      );
    } else if (count == 4) {
      return Scaffold(
        body: Center(
            child: GestureDetector(
          onTap: () {
            flag = false;
          },
          child: Container(
            height: 200,
            width: 300,
            decoration: const BoxDecoration(color: Colors.black, boxShadow: [
              BoxShadow(
                  offset: Offset(0, -10), color: Colors.red, blurRadius: 20)
            ]),
          ),
        )),
      );
    } else {
      return Scaffold(
        body: Center(
          child: Container(
            height: 300,
            width: 300,
            decoration: const BoxDecoration(
              shape: BoxShape.circle,
              gradient: LinearGradient(
                stops: [0.50, -1],
                colors: [Colors.black, Colors.red],
              ),
            ),
          ),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      body: question(),
      floatingActionButton: FloatingActionButton(
        onPressed: nextQuestion,
        child: const Text("Next"),
      ),
    ));
  }
}
